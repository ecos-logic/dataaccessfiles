package org.ecos.logic.data;

import java.io.Serializable;

public class Book implements Serializable {
    private final Integer year;
    private final Float rating;
    private final String name;
    private final String author;

    public Book(Integer year, Float rating, String name, String author) {

        this.year = year;
        this.rating = rating;
        this.name = name;
        this.author = author;
    }

    public Integer getYear() {
        return year;
    }

    public Float getRating() {
        return rating;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    @Override
    public String toString() {
        return "Book{" +
                "year=" + year +
                ", rating=" + rating +
                ", name='" + name + '\'' +
                ", author='" + author + '\'' +
                '}';
    }
}
