package org.ecos.logic;

import org.ecos.logic.data.Book;

import java.io.*;
import java.util.*;

public class App {
    public static void main(String[] args) throws IOException {
        List<Book> bookList = readFrom(Objects.requireNonNull(App.class.getResource("/Libros.txt")).getPath());
        bookList.forEach(System.out::println);

        transformToBinary(bookList);
    }

    private static void transformToBinary(List<Book> bookList) throws IOException {
        try (ObjectOutputStream writer = new ObjectOutputStream(new FileOutputStream("Libros.dat"))) {
            for (Book book : bookList) {
                writer.writeObject(book);
            }
        }
    }

    private static List<Book> readFrom(String filePath) throws FileNotFoundException {
        List<Book> bookList = new ArrayList<>();
        try (Scanner scanner = new Scanner(new File(filePath))) {
            while (scanner.hasNextLine()) {
                String bookLine = scanner.nextLine();
                List<String> lineSeparated = Arrays.stream(bookLine.split("\t")).toList();
                Book book = new Book(Integer.valueOf(lineSeparated.get(0)), Float.valueOf(lineSeparated.get(1)), lineSeparated.get(2), lineSeparated.get(3));
                bookList.add(book);
            }
        }
        return bookList;
    }
}
